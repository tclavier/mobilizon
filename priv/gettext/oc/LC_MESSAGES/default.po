msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-09-24 14:40+0000\n"
"PO-Revision-Date: 2019-10-17 14:31+0000\n"
"Last-Translator: Quentin <quentinantonin@free.fr>\n"
"Language-Team: Occitan <https://weblate.framasoft.org/projects/mobilizon/"
"backend/oc/>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 3.9\n"

#: lib/mobilizon_web/templates/email/password_reset.html.eex:48
#: lib/mobilizon_web/templates/email/password_reset.text.eex:12
#, elixir-format
msgid "If you didn't request this, please ignore this email. Your password won't change until you access the link below and create a new one."
msgstr ""
"S’avètz pas demandat aquò, podètz ignorar aqueste corrièl. Vòstre senhal cambiarà pas mentre que cliquetz pas lo ligam çai-jos e ne definiscatz un novèl."

#: lib/service/export/feed.ex:169
#, elixir-format
msgid "Feed for %{email} on Mobilizon"
msgstr "Flux per %{email} sus Mobilizon"

#: lib/mobilizon_web/templates/email/email.html.eex:153
#: lib/mobilizon_web/templates/email/email.text.eex:6
#, elixir-format
msgid "%{instance} is a Mobilizon server."
msgstr "%{instance} es una instància Mobilizon."

#: lib/mobilizon_web/templates/email/report.html.eex:38
#, elixir-format
msgid "%{reporter_name} (%{reporter_username}) reported the following content."
msgstr "%{reporter_name} (%{reporter_username}) a senhalat lo contengut seguent."

#: lib/mobilizon_web/templates/email/report.html.eex:48
#, elixir-format
msgid "%{title} by %{creator}"
msgstr "%{title} per %{creator}"

#: lib/mobilizon_web/templates/email/registration_confirmation.html.eex:58
#, elixir-format
msgid "Activate my account"
msgstr "Activar mon compte"

#: lib/mobilizon_web/templates/email/email.html.eex:122
#, elixir-format
msgid "Ask the community on Framacolibri"
msgstr "Demandar a la comunautat sus Framacolibri"

#: lib/mobilizon_web/templates/email/report.html.eex:62
#: lib/mobilizon_web/templates/email/report.text.eex:11
#, elixir-format
msgid "Comments"
msgstr "Comentaris"

#: lib/mobilizon_web/templates/email/report.html.eex:46
#: lib/mobilizon_web/templates/email/report.text.eex:6
#, elixir-format
msgid "Event"
msgstr "Eveniment"

#: lib/mobilizon_web/templates/email/registration_confirmation.html.eex:45
#, elixir-format
msgid "If you didn't request this, please ignore this email."
msgstr "S’avètz pas demandat aquò, mercés d’ignorar aqueste messatge."

#: lib/mobilizon_web/email/user.ex:45
#, elixir-format
msgid "Instructions to reset your password on %{instance}"
msgstr "Consignas per reïnincializar vòstre senhal sus %{instance}"

#: lib/mobilizon_web/templates/email/email.html.eex:154
#, elixir-format
msgid "Learn more about Mobilizon."
msgstr "Ne saber mai tocant Mobilizon."

#: lib/mobilizon_web/templates/email/registration_confirmation.html.eex:13
#, elixir-format
msgid "Nearly here!"
msgstr "I sètz gaireben !"

#: lib/mobilizon_web/templates/email/email.html.eex:119
#, elixir-format
msgid "Need some help? Something not working properly?"
msgstr "Besonh d’ajuda ? Quicòm truca ?"

#: lib/mobilizon_web/templates/email/report.html.eex:13
#, elixir-format
msgid "New report on %{instance}"
msgstr "Nòu senhalament sus %{instance}"

#: lib/mobilizon_web/templates/email/report.html.eex:80
#: lib/mobilizon_web/templates/email/report.text.eex:18
#, elixir-format
msgid "Reason"
msgstr "Rason"

#: lib/mobilizon_web/templates/email/password_reset.html.eex:61
#, elixir-format
msgid "Reset Password"
msgstr "Reïnicializar mon senhal"

#: lib/mobilizon_web/templates/email/password_reset.html.eex:41
#, elixir-format
msgid "Resetting your password is easy. Just press the button below and follow the instructions. We'll have you up and running in no time."
msgstr "Reïnicializar vòstre senhal es facil. Clicatz simplament lo boton e seguètz las consignas. Seretz prèst d’aquí un momenton."

#: lib/mobilizon_web/templates/email/password_reset.html.eex:13
#, elixir-format
msgid "Trouble signing in?"
msgstr "De dificultats a vos connectar ?"

#: lib/mobilizon_web/templates/email/report.html.eex:100
#, elixir-format
msgid "View the report"
msgstr "Veire lo senhalament"

#: lib/mobilizon_web/templates/email/registration_confirmation.html.eex:38
#, elixir-format
msgid "You created an account on %{host} with this email address. You are one click away from activating it."
msgstr "Avètz creat un compte sus %{host} amb aquesta adreça electronica. Sètz a un clic de l’activar."

#: lib/mobilizon_web/email/user.ex:25
#, elixir-format
msgid "Instructions to confirm your Mobilizon account on %{instance}"
msgstr "Consignas per confirmar vòstre compte Mobilizon sus %{instance}"

#: lib/mobilizon_web/email/admin.ex:23
#, elixir-format
msgid "New report on Mobilizon instance %{instance}"
msgstr "Nòu senhalament sus l’instància Mobilizon %{instance}"

#: lib/mobilizon_web/templates/email/registration_confirmation.text.eex:1
#, elixir-format
msgid "Activate your account"
msgstr "Activar mon compte"

#: lib/mobilizon_web/templates/email/event_participation_approved.html.eex:13
#, elixir-format
msgid "All good!"
msgstr "Aquò’s tot bon !"

#: lib/mobilizon_web/templates/email/event_participation_approved.html.eex:45
#: lib/mobilizon_web/templates/email/event_participation_approved.text.eex:7
#, elixir-format
msgid "An organizer just approved your participation. You're now going to this event!"
msgstr "L’organizator ven d’aprovar vòstra participacion. Ara anatz a aqueste eveniment !"

#: lib/mobilizon_web/templates/email/event_participation_approved.html.eex:58
#: lib/mobilizon_web/templates/email/event_updated.html.eex:101
#, elixir-format
msgid "Go to event page"
msgstr "Anar a la pagina de l’eveniment"

#: lib/mobilizon_web/templates/email/event_participation_approved.html.eex:70
#: lib/mobilizon_web/templates/email/event_updated.html.eex:113
#: lib/mobilizon_web/templates/email/event_updated.text.eex:21
#, elixir-format
msgid "If you need to cancel your participation, just access the event page through link above and click on the participation button."
msgstr ""
"Se vos fa besonh d’anullar vòstra participacion, vos cal pas qu’accedir a la pagina de l’eveniment via lo ligam çai-jos e clicar lo boton de "
"participacion."

#: lib/mobilizon_web/templates/email/event_participation_approved.text.eex:11
#, elixir-format
msgid "If you need to cancel your participation, just access the previous link and click on the participation button."
msgstr "Se vos fa besonh d’anullar vòstra participacion, vos cal pas qu’accedir al ligam çai-jos e clicar lo boton de participacion."

#: lib/mobilizon_web/templates/email/email.text.eex:6
#, elixir-format
msgid "Learn more about Mobilizon:"
msgstr "Ne saber mai tocant Mobilizon :"

#: lib/mobilizon_web/templates/email/report.text.eex:1
#, elixir-format
msgid "New report from %{reporter} on %{instance}"
msgstr "Nòu senhalament sus %{instance}"

#: lib/mobilizon_web/templates/email/event_participation_approved.text.eex:1
#, elixir-format
msgid "Participation approved"
msgstr "Participacion aprovada"

#: lib/mobilizon_web/templates/email/event_participation_rejected.text.eex:1
#, elixir-format
msgid "Participation rejected"
msgstr "Participacion regetada"

#: lib/mobilizon_web/templates/email/password_reset.text.eex:1
#, elixir-format
msgid "Password reset"
msgstr "Reïnicializacion del senhal"

#: lib/mobilizon_web/templates/email/password_reset.text.eex:7
#, elixir-format
msgid "Resetting your password is easy. Just click the link below and follow the instructions. We'll have you up and running in no time."
msgstr "Reïnicializar vòstre senhal es facil. Clicatz simplament lo boton e seguètz las consignas. Seretz prèst d’aquí un momenton."

#: lib/mobilizon_web/templates/email/event_participation_rejected.html.eex:13
#, elixir-format
msgid "Sorry!"
msgstr "Nos dòl !"

#: lib/mobilizon_web/templates/email/event_participation_rejected.html.eex:45
#: lib/mobilizon_web/templates/email/event_participation_rejected.text.eex:7
#, elixir-format
msgid "Unfortunately, the organizers rejected your participation."
msgstr ""
"Malaürosament, los organizaires an regetada vòstra demanda de participacion."

#: lib/mobilizon_web/templates/email/registration_confirmation.text.eex:5
#, elixir-format
msgid "You created an account on %{host} with this email address. You are one click away from activating it. If this wasn't you, please ignore this email."
msgstr "Avètz creat un compte sus %{host} amb aquesta adreça electronica. Sètz a un clic de l’activar."

#: lib/mobilizon_web/templates/email/event_participation_approved.html.eex:38
#, elixir-format
msgid "You requested to participate in event %{title}"
msgstr "Avètz demandat de participar a l’eveniment %{title}"

#: lib/mobilizon_web/templates/email/event_participation_approved.text.eex:5
#: lib/mobilizon_web/templates/email/event_participation_rejected.html.eex:38
#: lib/mobilizon_web/templates/email/event_participation_rejected.text.eex:5
#, elixir-format
msgid "You requested to participate in event %{title}."
msgstr "Avètz demandat de participar a l’eveniment %{title}."

#: lib/mobilizon_web/email/participation.ex:73
#, elixir-format
msgid "Your participation to event %{title} has been approved"
msgstr "Vòstra participacion a l’eveniment %{title} es estada aprovada"

#: lib/mobilizon_web/email/participation.ex:52
#, elixir-format
msgid "Your participation to event %{title} has been rejected"
msgstr "Vòstra participacion a l’eveniment %{title} es estada regetada"

#: lib/mobilizon_web/templates/email/event_updated.html.eex:82
#, elixir-format
msgid "Ending of event"
msgstr "Fin de l’eveniment"

#: lib/mobilizon_web/email/event.ex:30
#, elixir-format
msgid "Event %{title} has been updated"
msgstr "L’eveniment %{title} es estat actualizat"

#: lib/mobilizon_web/templates/email/event_updated.html.eex:13
#: lib/mobilizon_web/templates/email/event_updated.text.eex:1
#, elixir-format
msgid "Event updated!"
msgstr "Eveniment actualizat !"

#: lib/mobilizon_web/templates/email/event_updated.text.eex:16
#, elixir-format
msgid "New date and time for ending of event: %{ends_on}"
msgstr "Novèla data e ora de fin de l’eveniment : %{ends_on}"

#: lib/mobilizon_web/templates/email/event_updated.text.eex:12
#, elixir-format
msgid "New date and time for start of event: %{begins_on}"
msgstr "Novèla data e ora de debuta de l’eveniment : %{begins_on}"

#: lib/mobilizon_web/templates/email/event_updated.text.eex:8
#, elixir-format
msgid "New title: %{title}"
msgstr "Títol novèl : %{title}"

#: lib/mobilizon_web/templates/email/event_updated.html.eex:72
#, elixir-format
msgid "Start of event"
msgstr "Debuta de l’eveniment"

#: lib/mobilizon_web/templates/email/event_updated.text.eex:5
#, elixir-format
msgid "The event %{title} was just updated"
msgstr "L’eveniment %{title} es estat actualizat"

#: lib/mobilizon_web/templates/email/event_updated.html.eex:38
#, elixir-format
msgid "The event %{title} was updated"
msgstr "L’eveniment %{title} es estat actualizat"

#: lib/mobilizon_web/templates/email/event_updated.html.eex:62
#, elixir-format
msgid "Title"
msgstr "Títol"

#: lib/mobilizon_web/templates/email/event_updated.text.eex:19
#, elixir-format
msgid "View the updated event on: %{link}"
msgstr "Veire l’eveniment actualizat sus : %{link}"

#: lib/mobilizon_web/templates/email/password_reset.html.eex:38
#: lib/mobilizon_web/templates/email/password_reset.text.eex:5
#, elixir-format
msgid "You requested a new password for your account on %{instance}."
msgstr "Avètz demandat un nòu senhal per vòstre compte sus %{instance}."

#: lib/mobilizon_web/templates/email/email.html.eex:91
#, elixir-format
msgid "%{b_start}Please do not use it in any real way%{b_end}: everything you create here (accounts, events, identities, etc.) will be automatically deleted every 48 hours."
msgstr ""
"%{b_start}Mercés de far pas una utilizacion reala%{b_end} : tot çò que "
"creatz aquí (compte, eveniments, identitats, etc.) serà suprimit cada 48 "
"oras."

#: lib/mobilizon_web/templates/email/email.html.eex:94
#, elixir-format
msgid "In the meantime, please consider that the software is not (yet) finished. More information %{a_start}on our blog%{a_end}."
msgstr ""
"D’aquel temps, consideratz que lo logicial es pas (encara) acabat. Mai d’"
"informacion %{a_start}sus nòstre blòg%{a_end}."

#: lib/mobilizon_web/templates/email/email.html.eex:93
#, elixir-format
msgid "Mobilizon is under development, we will add new features to this site during regular updates, until the release of %{b_start}version 1 of the software in the first half of 2020%{b_end}."
msgstr ""
"Mobilizon es en desvolopament, ajustarem de nòvas foncionalitats a aqueste "
"site pendent de mesas a jorn regularas, fins a la publicacion de %{b_start}"
"la version 1 del logicial al primièr semèstre 2020%{b_end}."

#: lib/mobilizon_web/templates/email/email.html.eex:90
#, elixir-format
msgid "This is a demonstration site to test the beta version of Mobilizon."
msgstr ""
"Aquò es un site de demostracion per ensajar la version beta de Mobilizon."

#: lib/mobilizon_web/templates/email/email.html.eex:88
#, elixir-format
msgid "Warning"
msgstr "Avertiment"

#: lib/mobilizon_web/templates/email/event_updated.html.eex:54
#, elixir-format
msgid "Event has been cancelled"
msgstr "L’eveniment es estat anullat"

#: lib/mobilizon_web/templates/email/event_updated.html.eex:50
#, elixir-format
msgid "Event has been confirmed"
msgstr "L’eveniment es estat confirmat"

#: lib/mobilizon_web/templates/email/event_updated.html.eex:52
#, elixir-format
msgid "Event status has been set as tentative"
msgstr "L’estatut de l’eveniment es estat definit coma « de confirmar »"
